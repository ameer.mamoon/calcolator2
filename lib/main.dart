
import 'package:calculator/Screens/Scientific.dart';
import 'package:flutter/material.dart';

import 'Screens/Standard.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      themeMode: ThemeMode.system,
      home: Scaffold(
        body: SafeArea(
          child: OrientationBuilder(
            builder: (context, orientation){
              if(orientation == Orientation.portrait)
                return StandardScreen();
              else
                return ScientificScreen();
            }
          ),
        ),
      ),
      );
  }
}
