// ignore_for_file: file_names

import 'package:flutter/material.dart';

import '../elements.dart';



class StandardScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(

        children: [
          Expanded(
            child: OutputWidget(CalcButton.controller),
            flex: 2,
          ),
            CalcRow(['=','DEL']),
            CalcRow(['7','8','9','+']),
            CalcRow(['4','5','6','-']),
            CalcRow(['1','2','3','×']),
            CalcRow(['.','0','C','÷']),

        ],
      ),
    );
  }

}



class CalcRow extends StatelessWidget {
  final List<Widget> _children = [];
  final List<String> content;

  CalcRow(this.content);

  @override
  Widget build(BuildContext context) {
    for(int i = 0; i < content.length ;i++){
      _children.add(
        CalcButton(content[i])
      );
    }

    return Expanded(
      child: Row(
        children: _children,
      ),
    );
  }
}
