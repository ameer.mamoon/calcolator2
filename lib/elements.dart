import 'package:calculator/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CalcButton extends StatelessWidget {
  final String content;

  static final calcController controller = Get.put(calcController());

  CalcButton(this.content);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Expanded(
      child: GestureDetector(
        child: Container(
          width: double.infinity,
          height: double.infinity,

          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(size.width*0.02),
          ),
          margin: EdgeInsets.all(size.width*0.01),

          child: Center(
            child: Text(content),
          ),
        ),
        onTap: (){
          controller.press(content);
        },
      ),
    );
  }
}

class OutputWidget extends StatelessWidget {

  final calcController controller;
  OutputWidget(this.controller);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return GetBuilder<calcController>(
      init: controller,
      builder: (c){
        return Container(
          padding: EdgeInsets.all(width * 0.035),
          child: Column(
            children: [
              NumberArea(1, c.subContent),
              NumberArea(2, c.content),
            ],
          ),
        );
      },
    );
  }
}

class NumberArea extends StatelessWidget {
  final int flex ;
  final String text ;

  NumberArea(this.flex, this.text);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flex,
      child: LayoutBuilder(
        builder: (context,constrains) {
          return Container(
            padding: EdgeInsets.all(constrains.maxHeight*0.075),
            width: double.infinity,
            height: double.infinity,
            child: FittedBox(
              child: Text(text),
              alignment: Alignment.centerLeft,
            ),
          );
        }
      ),
    );
  }
}

